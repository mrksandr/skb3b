import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const app = express();

const corsOptions = {
  origin: 'http://account.skill-branch.ru',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

const petsUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

let api = {};

fetch(petsUrl)
	.then(async (res) => {
		if (res.status >= 400) {
			throw new Error('Bad response from server');
		}

		api = await res.json();
	})
	.catch(err => {
		console.log('Чтото пошло не так:', err);
	});

function catchError(error) {
	switch (error.message) {
		case 'Not Found':
			res.sendStatus('404').send('Not Found');
			break;
		default:
			res.send(error.message);
	}
}


app.get('/', (req, res) => {
	try {

		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(api));

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users', (req, res) => {
	try {
		const
			havePet = req.query.havePet,
			users = api.users,
			pets = api.pets;

		if (havePet) {
			res.json(_.filter(users, (user) => {
				return _.size(_.find(pets, {type: havePet, userId: +user.id})) > 0;
			}));
		}
		res.json(users);

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users/populate', (req, res) => {
	try {
		const
			havePet = req.query.havePet,
			pets = api.pets,
			users = api.users.map(user => Object.assign({}, user, {pets: _.filter(pets, {userId: +user.id})}));

		if (havePet) {
			res.json(_.filter(users, (user) => {
				return _.size(_.find(pets, {type: havePet, userId: +user.id})) > 0;
			}));
		}

		 res.json(users);

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});


app.get('/users/:id', (req, res, next) => {
	try {
		const
			id = req.params.id,
			users = api.users,
			user = _.find(users, ['id', +id]);

		if (!user) next();
		res.json(user);

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users/:id/populate', (req, res, next) => {
	try {
		const
			id = req.params.id,
			users = api.users,
			pets = api.pets;

		let user = _.find(users, {id: +id});

		if (!user) next();

		user = Object.assign({}, user, {pets: _.filter(pets, {userId: +user.id})});

		res.json(user);
	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});


app.get('/users/:username', (req, res, next) => {
	try {
		const
			username = req.params.username,
			users = api.users,
			user = _.find(users, {username});

		if (!user) {
			throw new Error('Not Found');
		} else {
			res.json(user);
		}

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users/:username/populate', (req, res, next) => {
	try {
		const
			username = req.params.username,
			users = api.users,
			pets = api.pets;

		let user = _.find(users, {username});

		if (!user) {
			throw new Error('Not Found');
		} else {
			user = Object.assign({}, user, {pets: _.filter(pets, {userId: +user.id})});
			res.json(user);
		}

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users/:id/pets', (req, res, next) => {
	try {
		const
			id = req.params.id,
			users = api.users,
			user = _.find(users, ['id', +id]);

		if (!user) next();

		const pets = _.find(api.pets, {userId: +user.id});
		res.json(pets);

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/users/:username/pets', (req, res, next) => {
	try {
		const
			username = req.params.username,
			users = api.users,
			user = _.find(users, {username});

		if (!user) {
			throw new Error('Not Found');
		} else {
			const pets = _.find(api.pets, {userId: +user.id});
			res.json(pets);
		}

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.get('/pets', (req, res) => {
	try {

		const query = req.query;
		let pets = api.pets;

		if (query.type) {
			pets = _.filter(pets, {type: query.type});
		}
		if (query.age_gt) {
			pets = _.filter(pets, pet =>  pet.age > query.age_gt);
		}
		if (query.age_lt) {
			pets = _.filter(pets, pet =>  pet.age < query.age_lt);
		}

		res.json(pets);

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});


app.get('/pets/:id', (req, res) => {
	try {

		const
			id = req.params.id,
			pets = api.pets,
			pet = _.find(pets, {id: +id});

		if (!pet) {
			throw new Error('Not Found');
		} else {
			res.json(pet);
		}

	} catch (err) {
		switch (err.message) {
			case 'Not Found':
				res.sendStatus('404').send('Not Found');
				break;
			default:
				res.send(err.message);
		}
	}
});

app.listen(3000, () => {
	console.log('Example app listening on port 3000!');
});